# HTML (HyperText Markup Language)

- Sommaire : 

    - [Définition](#définition-)
    - [Sectionnement du contenu](#sectionnement-du-contenu)
    - [Contenu textuel](#contenu-textuel)
    - [Métadonnées du document](#métadonnées-du-document)
   
#### Définition :

c'est un Langage de balisage utilisé pour la création des pages web, permettant notamment de définir des liens hypertextes.

___Racine principale___

``<html>`` représente la racine d'un document HTML. 

___Racine de sectionnement___

``<body>`` représente le contenu principal du document HTML.

### Sectionnement du contenu

#### _*Organiser le contenu d'une page en différentes sections permet d'avoir une structure logique au sein d'un document.*_

``<article>`` représente du contenu autonome dans un document, une page, une application, ou un site.

``<section>`` représente une section générique d'un document.

``<header>``	 représente un groupe de contenu introductif ou de contenu aidant à la navigation.

``<h1>, <h2>, <h3>, <h4>, <h5>, <h6>`` représentent six niveaux de titres dans un document.

``<main>`` représente le contenu majoritaire du <body> du document.

``<footer>``	 représente le pied de page de la section ou de la racine de sectionnement la plus proche.

``<address>``	indique des informations de contact pour une personne.

``<hgroup>`` représente un titre de plusieurs niveaux pour une section d'un document.

``<nav>`` représente une section d'une page ayant des liens vers d'autres pages ou des fragments de cette page.
 
``<aside>`` représente une partie d'un document dont le contenu n'a qu'un rapport indirect avec le contenu principal du document.

### Contenu textuel

#### _*Le contenu HTML textuel permet d'organiser des blocs ou des sections de contenu entre la balise ouvrante ``<body>`` et la balise fermante ``</body>``.*_

```<p>```	 représente un paragraphe de texte.

``<div>`` (qui signifie division du document) est un conteneur générique qui permet d'organiser le contenu sans représenter rien de particulier.

```<ul>```	 représente une liste d'éléments sans ordre particulier.

```<li>```	 est utilisé pour représenter un élément dans une liste. 

```<hr>```	 représente un changement thématique entre des éléments de paragraphe.

```<ol>```	représente une liste ordonnée. 

```<main>```	 représente le contenu majoritaire du body du document.

``<blockquote>`` indique que le texte contenu dans l'élément est une citation longue.

```<dd>```	  indique la définition d'un terme au sein d'une liste de définitions. 

```<dir>```	 (pour directory) est utilisé comme un conteneur pour un répertoire.

```<dl>```	représente une liste de descriptions sous la forme d'une liste de paires associant des termes. 

```<dt>```	identifie un terme dans une liste de définitions ou de descriptions.

```<figcaption>``` permet d'indiquer un sous-titre.

```<figure>```	 représente une figure (un schéma).

```<pre>```	 représente du texte préformaté.

### Métadonnées du document

#### _*Les métadonnées contiennent des informations à propos de la page.*_

``<title>``	 définit le titre du document (qui est affiché dans la barre de titre du navigateur ou dans l'onglet de la page).

``<head>``	 fournit des informations générales (métadonnées) sur le document.

``<link>``	 définit la relation entre le document courant et une ressource externe.

``<style>``	 contient des informations de mise en forme pour un document ou une partie d'un document.

``<base>`` définit l'URL de base à utiliser pour recomposer toutes les URL relatives contenues dans un document. 


