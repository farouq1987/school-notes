# Git

## Présentation

gitlab est un site internet

git est un protocole qui permet de changer les données entre developpeurs


### 1er partie

créer un compte "gitlab"


 __*Instalation:*__ 


1. dans 'webstorm File' on selection 'settings'

1. dans 'settins' on selection 'plugins'(plugins=stor webstorm)

1. on choisissant le programe GitLab Projects (pavel polivka) 

___*Comment envoyer un projet dans un repository:*___


une fois gitlab projects installé:

1.  "git init" dans le terminal= (initialisation du protocole git)une flèche bleue et verte apparait et le dossier apparait en rouge ce qui veut dire que il est pas pris en compte par git

2. ajouter un dossier a git(clique droit sur le dossier et on selection 'git' et aprés 'add'= (ajouter le dossie a git) et le dossier apparait en vert)

3. on clique sur la flèche 'verte'= (commit) et on rajoute un commit message =(message de l'opération)

4. commit and push

on clique sur 'commit and pish'

 on sauvegarde sur la machine et on envoyoi  les modifications 
 
  __*Parametres git lab*__
 
 * add new gitlab serveur on rempli les informations demandées

### 2eme partie

__*Configuration de git*__

(on fait cette etape une seul et unique fois)

* dans le terminal

* git config --global user.name "FIRST8NAME LAST8NAME"

* git config --global user.email"l'adresse email qui est enregistrée sur gitlab projects

apres sur webstorm icon file version control on tape sur le store puis on installe le logiciel on redemmare webstorme


### 3eme partie 

__*création de repository*__


créer un REPOSITORY(dossier) l'emplacement de notre projet sur gitlab

* example: my project (school notes)

dans le site gitlab

 *new project* 

* créer un projet on choisit un projet vide (blank project)

* on donne un nom au projet

* clone

*Git ignore* (eviter d'envoyer certains éléments inutiles sur git;Exemple:_nodemodule, package.json etc_) 
 
 
 1. Créer un fichier dans la racine appelé .gitignore puis l'ajouter a git (.nodemodule par exemple)
 2. Dans ce fichier, mettre tous les fichiers et dossiers que l'on ne veut pas en commencant par un .
 3. Pour retirer un fichier (idea par exemple) qui n'a pas été ignoré, Saisir git rm -rf .idea
 4. On refait commit, idea aura disparu

* on copie l'url de projet

* Sur Webtorm, cliquer sur 'define remote'

* Coller l'url dans 'define remote' et on  clique sur 'push'

__*recupérer un projet de git vers webstorm*__

1. copier l' (url) de projet
2. coller l'(url) 'clone' dans webstorm au VCS __get from version control__
3. Cliquer sur clone sur git et copier le lien https

__*commit*__

1. avec la flèche verte on ouvre la fenêtre
2. on indique un message pour la raison de commit
3. commit and push

### 4eme partie 

__*les branches*__

 **master**
 
 * la version de référence du document principal du logiciel avec tous les fichiers qui sont acceptés par le chef de projet
  si on travaille en equipe on doit faire des branches (copies de la  master)
  
 __*créaation d'une branche*__
 
 1. on clique sur master
 2. on donne un nom à la branche (uniquement en Anglais)
 
>* checkout ( c'est de passer d'une branche à l'autre mais on doit comiter en premier avant de changer la branche)
>* apres la création de la branche on doit demander l'autorisation d'envoyer notre branche avec  les modifications faites  sur le projet
>* dans l'espace (gitlab) la page principale du projet
>* on selectionne __(create merge request)__c'est à dire demander de mettre ensemble votre branche avec la branche principale *master*
>* on peut aussi selectionner (create merge request) à une autre branche autre que master
>* demander l'autorisation du chef de projet (la branche master) pour prendre en compte les modifications qui sont faites sur le projet et les envoyer vers la master

 avec le bouton __submit merge request__
 
 __*recuperer les modifications*__
 
 on passe dans la branche master puis on clique sur la flèche bleue. Une fenêtre apparait on selectionne __merge__.


 