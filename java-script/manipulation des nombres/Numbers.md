# Manipulation des nombres

* Manipuler des chiffres a virgule est complexe en Javascript.

* Les outils mathématique s'utilisent avec la librairie math.

[Math.round](#mathround)

[Math.floor](#mathfloor)

[toFixed](#tofixed)

[parseInt](#parseint)

[parseFloat](#parsefloat)

## Math.round

La fonction _Math.round()_ retourne la valeur d'un nombre arrondi à l'entier le plus proche.

*Exemple: 1*

```javascript
console.log(Math.round(9.9));
```

La console va renvoyer la rusultat (10). 


```javascript
console.log(Math.round(9.4));
```
La console va renvoyer la rusultat (9).

A partir de 9.5 la fonction renvoit le nombre au dessus.

*Exemple: 2*

Il renvoit un nombre aléatoire compris entre 0 et 1 (le 1 n'est pas compris).

```javascript
console.log(Math.random());
```

La console va renvoyer 0.06123456743.

A partir de ce nombre on va pouvoir fabriquer des nombres aléatoires.

```javascript
console.log(Math.random() * 1000 + 1);
```

La console va renvoyer 859.196424784. Il envoit un nombre compris entre 1 et 1000 (1000 non compris).

## Math.floor


La fonction _Math.floor(x)_ renvoie le plus grand entier qui est inférieur ou égal à un nombre.

```javascript
console.log(Math.floor(9.5);
```

La console va renvoyer (9) Il va garder que ce qu'il y a avant la virgule.

 _**Créer un nombre aléatoire entre 2 nombres**_

```javascript
function createRandomNumber(min, max) {
    return Math.floor(min + Math.random() * (max - min) + 1);
}
console.log(createRandomNumber(10,15);
```

La console va renvoyer un nombre aléatoire entre 10 et 15.

## toFixed

La méthode _toFixed()_ permet de formater un nombre en notation à point-fixe.

```javascript
console.log(10.66668787878.toFixed(2));
```

La console va renvoyer 10.67

 Le 2 après toFixed indique le nombre de virgule voulue.

Le problème c'est que il renvoit un string et non un nombre.


## parseInt

La fonction _parseInt()_ analyse une chaîne de caractère fournie en argument et renvoie un entier exprimé dans une base donnée.

## parseFloat

La fonction _parseFloat()_ permet de transformer une chaîne de caractères en un nombre flottant après avoir analysée celle-ci (parsing).

```javascript
console.log(parseFloat('10.67') + 10);
```
La console va renvoyer 20.67.

On peut l'utiliser avec la méthode toFixed :

```javascript
console.log(parseFloat(10.6777.toFixed(2)) + 10);
```

La console va renvoyer 20.68.
