# Destructuration des objets

* La Destructuration permet de récupérer des variables dans un Array ou un Object et de leur donner un nom.


**_méthode traditionelle_**
 
 
```javascript
 const user = {
    firstName: 'momo",
    lastName: 'far',
    age: 44,
    billionaire: false,
    eyes: 'blue'
};

const age = user.age;
const lastName = user.lastName;

console.log(age, lastName);
```
La console va renvoyer l'age et le nom.


**_Nouvelle méthode_**


```javascript
const user = {
    firstName: 'momo',
    lastName: 'far',
    age: 34,
    billionaire: false,
    eyes: 'blue'
};

const {age, lastName} = user;

console.log(age, lastName);
```
