# le nom des attributs

### ajouter des attributs a un objet

```javascript
const user = {
    firstName: 'momo',
    lastName: 'far',
    age: 34,
    billionaire: false,
};
```
````javascript
user.adress = '2 rue des fleurs',
````

On peut aussi rajouter l'adresse directement dans l'objet à la suite.

```javascript
const user = {
    firstName: 'momo',
    lastName: 'far',
    age: 34,
    billionaire: false,
    adress : '2 rue des fleurs',
};
```
### Avoir accès a un attributs inconnu

C'est une notation qui permet d'avoir accès a l'attribut d'un objet quand on ne sait pas le nom de cet attribut a l'avance.

```javascript
const attrs = ['lastName', 'age'];

attrs.forEach(attr => console.log(user[attr]))
```
l'élément entre les crochets n'existe pas, il va aller récuperer ce qu'on demande dans la const attrs.

Il va aller chercher lastName d'abord, puis l'âge et les afficher un par un.

