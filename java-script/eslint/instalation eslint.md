## Trouver des problèmes

ESLint analyse statiquement votre code pour trouver rapidement des problèmes. ESLint est intégré à la plupart des éditeurs de texte et vous pouvez exécuter ESLint dans le cadre de votre pipeline d'intégration continue.

## Réparer automatiquement

De nombreux problèmes détectés par ESLint peuvent être résolus automatiquement. Les correctifs ESLint sont sensibles à la syntaxe afin que vous ne rencontriez pas d'erreurs introduites par les algorithmes traditionnels de recherche et remplacement.

## Personnaliser

Prétraitez le code, utilisez des analyseurs personnalisés et écrivez vos propres règles qui fonctionnent avec les règles intégrées d'ESLint. Vous pouvez personnaliser ESLint pour qu'il fonctionne exactement comme vous en avez besoin pour votre projet.
