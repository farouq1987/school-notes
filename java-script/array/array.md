# Tableaux (Array)

L'objet global **Array** [] est utilisé pour créer des tableaux []. Les tableaux sont des objets de haut-niveau semblables à des listes.

- sommaire : 

    - [Length](#length) 
    - [forEach](#foreach)
    - [map](#map)
    - [filter](#filter)
    - [sort](#sort) 
    - [includes](#includes)
    - [push](#push)
    - [concat](#concat)
    - [splice](#splice)
    - [slice](#slice)
    - [reduce](#reduce)
    - [indexOf](#indexof)
    - [find](#find)
    - [evry&some](#every-some)
    
### length

La propriété __length__ (longueur) revoie un nombre qui indique le nombre d'éléments présents dans le tableau.

```javascript
const tabLength = [me,user1,user2,user3];
console.log(tabLength.length);
```

### forEach

La méthode __forEach()__(parcourire tout les élément des tableaux) il  permet d'exécuter une fonction donnée sur chaque élément du tableau.

```javascript
const tab1 = ['user1', 'user2', 'user3'];
tab1.forEach(function(element) {
});
  console.log('user1:',element);
```
ou 
```javascript
const tab1 = ['user1', 'user2', 'user3'];
tab1.forEach(element =>{
element.user1});
console.log(tab1);
```

### map

La méthode __map()__ crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant et ronvoyer ca valeur.

```javascript
const tab1 = ['user1', 'user2', 'user3'];
tab1.map(element =>{
return element
});
console.table(tab1);
```
avec __map()__ ya tj une valeur return

### filter
 
L'opérateur __filter()__ crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction callback.

```javascript
const tab1 = ['user1', 'user2', 'user3'];
const filterAgeUser =tab1
.filter(user =>user.age < 18);
console.table(tab1);
```

### sort

La méthode __sort()__ trie et organiser les éléments d'un tableau dans le même tableau et renvoie le tableau.

```javascript
const tab1 = ['user1', 'user2', 'user3'];
const sortAgeUser =tab1.sort( (user1,user2) =>
user1.age - user2.age );
console.table(sortAgeUser);
```
remarque: les operateurs tj a la ligne

### includes

L'opérateur __includes__ permet de trouver un objet précise
 
```javascript
const tab1 = ['user1', 'user2', 'user3'];
const sortAgeUser =tab1.sort( (name) =>
name.includes('b') );
console.table(sortAgeUser);
```

### push

La méthode __push()__ ajoute un ou plusieurs éléments à la fin d'un tableau et retourne la nouvelle taille du tableau.

```javascript
const tab2 =[];
const user1={
    name:'moha'
};
const user2={
    name:'simo'
};
tab2.push(user1);
tab2.push(user2);
console.log(tab2);
```

### concat

L'opérateur __concat()__ fabrique un nouveau tableau il ne modifier pas le tableau parent il est utilisée afin de fusionner un ou plusieurs tableaux en les concaténant. Cette méthode ne modifie pas les tableaux existants, elle renvoie un nouveau tableau qui est le résultat de l'opération
```javascript
const tab2 =[1,2,3];
const tab3 =[4,5,6];
const tab4 =tab2.concat(tab3);
console.log(tab4);
```
* une autre métode pratique:

```javascript
const tab2 =[1,2,3];
const tab3 =[4,5,6];
const tab4 =[...tab2,...tab3];
console.log(tab4);
```
### splice

La méthode __splice()__ modifie le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments au tableau.On peut ainsi vider ou remplacer une partie d'un tableau.

```javascript
const tab = [1,2,3,4,5,6];
tab.splice( 2, 4);
console.log(tab);
```
avec l'opérateur __splice()__ on peut rajouter et suprimer a la foit des objet dans un tableau

### slice

La méthode __slice()__ renvoie un tableau, contenant une copie superficielle d'une portion du tableau d'origine, la portion est définie par un indice de début et un indice de fin (non-compris). Le tableau original ne sera pas modifié.

```javascript
const tab = [1,2,3,4,5,6];
tab.slice(2,5);
console.log(tab);
```

### reduce

La méthode __reduce()__ applique une fonction qui est un « accumulateur » et qui traite chaque valeur d'une liste afin de la réduire à une seule valeur.

```javascript
const tabNumber = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
// 1 + 2 + 3 + 4
console.log(tabNumber.reduce(reducer));
// expected output: 10
// 5 + 1 + 2 + 3 + 4
console.log(tabNumber.reduce(reducer, 5));
const tabName = [a,b,c];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
// a+b+c
console.log(tabName.reduce(reducer));
// expected output: abc
```
### indexOf

La méthode __indexOf()__ renvoie le premier indice pour lequel on trouve un élément donné dans un tableau. Si l'élément cherché n'est pas présent dans le tableau, la méthode renverra -1.

```javascript
const beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];
console.log(beasts.indexOf('bison'));
// expected output: 1
// start from index 2
console.log(beasts.indexOf('bison', 2));
// expected output: 4
console.log(beasts.indexOf('giraffe'));
// expected output: -1
```

### find

La méthode __find()__ renvoie la valeur du premier élément trouvé dans le tableau qui respecte la condition donnée par la fonction de test passée en argument. Sinon, la valeur

il renvoie le premier objet trouver (c'est y a plusieures)

```javascript
hestel = [marmara,prince,];
const  hotelFind =hostel.find(hotel =>hotel.name === prince);
console.log(hotelFind);
// true
```
### every & some

#### every

La méthode __every()__ permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument. Cette méthode renvoie un booléen pour le résultat du test.

*remarque* : Cette méthode renvoie true pour n'importe quelle condition utilisée sur un tableau vide.

```javascript
const tabAgeUser = (currentValue) => currentValue < 40;

const tabAge = [1, 30, 39, 29, 10, 13];

console.log(tabAge.every(tabAgeUser));
// expected output: true
```
#### some

La méthode __some()__ teste si au moins un élément du tableau passe le test implémenté par la fonction fournie. Elle renvoie un booléen indiquant le résultat du test.

*remarque* : Cette méthode renverra false, quelle que soit la condition, si elle est utilisée sur un tableau vide.

```javascript
const tabAgeUser = (currentValue) => currentValue < 40;

const tabAge = [1, 100, 90, 70, 50, 41];

console.log(tabAge.every(tabAgeUser));
// expected output: true
 const tabAge = [80, 100, 90, 70, 50, 41];

console.log(tabAge.every(tabAgeUser));
// expected output: false
```


