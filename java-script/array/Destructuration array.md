# Destructuration des tablaux

* La Destructuration permet de récupérer des variables dans un Array ou un Object et de leur donner un nom.

#### Modifier un élément
```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
       // Index =    0         1         2           3         4
console.log(legumes);

legumes[2] = 'Concombre';

//Brocoli sélectionner remplacer par Concombre

console.log(legumes);
```
Le premier ``console.log`` ne contiendra pas Concombre car il a été placer avant la modification

#### Trouver un élément et l'intéger dans une variables


**_Mauvaise Méthode_**

```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
        
        // Index =    0         1         2           3         4

const findBrocoli = legumes[2];
const findAil = legumes[4];

console.log(findBrocoli, findAil);
````


**_Nouvelle Méthode_**

```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
        
        // Index =    0         1         2           3         4

       const [linkToPoivron, linkToSalade, linkToBrocoli, linkToArtichaut, linkToAil] = legumes;
 
// chaque élément de la constante sera lier à des éléments de la const legumes (peu importe le nom ou primitives)

console.log(linkToArtichaut);
```

Chaque élement est désormais inscrit dans des variable individuelle


Si (Par exp) il y a des espace à la place de 'linkToPoivron', rien ne sera égal a Poivron

