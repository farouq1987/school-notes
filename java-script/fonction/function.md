# function

Une fonction JavaScript est un bloc de code conçu pour effectuer une tâche particulière.

Une fonction JavaScript est exécutée lorsque "quelque chose" l'invoque (l'appelle).

*exemple*

*  methode 1 (Functions classique)

```javascript
function addition(x,y) {
};
function test(a,b) {
return a+b;
};
console.log(test());
console.log(addition());
```
* methode 2 (Arrow functions)

```javascript
const test = (a,b) =>{return a+b};
console.log(test(a,b)); 
```
