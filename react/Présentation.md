# REACT

* *_React_* (aussi appelé React.js ou ReactJS) est une bibliothèque (librery) JavaScript libre développée par Facebook depuis 2013 qui gére le DOM virtuel.
 
 Le but principal de cette bibliothèque est de faciliter la création d'application web monopage, via la création de composants dépendant d'un état et générant une page (ou portion) HTML à chaque changement d'état.

* *_React_* est une bibliothèque qui ne gère que l'interface de l'application

* *_React_* est créé par "Jordan Walke"
