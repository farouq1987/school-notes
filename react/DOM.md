# DOM

Un _DOM Virtuel_ est une représentation du DOM en JavaScript. Au lieu de générer le DOM lui-même comme avec un langage de templating, c'est-à-dire au lieu de dialoguer avec les API du navigateur pour construire le DOM, on ne génère qu'une arborescence d'objets JavaScript en mémoire.
