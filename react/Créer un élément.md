 ## Créer un élément avec React
```html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<div id="root"></div>
<script crossorigin src="https://unpkg.com/react@16.13.1/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.13.1/umd/react-dom.development.js"></script>
<script>
  const value = 'React';
  const version = React.version;
  const root = document.getElementById('root');
  const exampleDiv = React.createElement('div', {
    className: 'main',
    children: `Bienvenue dans l'initiation à ${value} (version ${version})`,
  });
  ReactDOM.render(exampleDiv, root);
</script>
</body>
</html>
```

### RECUPERER LE DOM D'UN ELEMENT PAR SON ID

`<div id="root"></div>`

* `const root = document.getElementById('root')`====> on récupère le DOM qui a id 'root' et on le stock dans la const root


### CREER UN ELEMENT (DIV) 

```javascript
const exampleDiv = React.createElement('div', {
    className: 'main',
    children: `Bienvenue dans l'initiation à ${value} (version ${version})`,
  });
  ReactDOM.render(exampleDiv, root);
```
* dans l'element root `<div id="root"></div>` on a crée un children, ca class et 'main' et son contenu et `Bienvenue dans l'initiation à React (version 16.13.1)`
* ``ReactDOM.render(exampleDiv, root)`` ====> on prend le Dom virtuel et on affiche (render) exampleDiv et root.
  
  
