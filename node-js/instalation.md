# Installation Node

## A quoi sert-il ?

- **Node.js** sert à faire du Javascript server side, et peut être utilisé dans des applications de bases de données,
créer des applications en temps réel, 
où le serveur a la possibilité de transmettre de l’information au client.
 
- **Node.js a de nombreux avantages :** 

    Système de paquet intégré (NPM), modèle non bloquant, performance du moteur V8, logiciel libre (licence MIT). 
    Il dispose également d'une communauté très active. Son principal atout est la possibilité de coder en **Javascript.** 

- On utilise **Node.js** pour faire des applications cross-plateforme avec des frameworks 
comme **Ionic** pour les téléphones ou encore **Electron** pour des ordinateurs portables. 
Il est aussi employé parfois pour faire des serveurs web. 

## L'Installation

- ...

- ...

`[étapes annexes pas encore montrées !]`

- faire un `cd server` avant tout

- puis `npm i` pour tous les fichier

- Installer `npm install -g concurrently` dans le terminal
> Il permet de réaliser plusieur tache en même temps !

- Clique droit sur `package.json` puis `Show NPM Script`

- Lancer `Watch` au tout début et attendre la fin de la compilation

> Une fois lancer pour la première fois, il ne sera plus utile de le lancer.

- Lancer `serve`. **Le watcher et le server seront ouvert à chaque lancement**

- Désormais, il y aura 2 fichiers server :
    - `server.ts` qui sera un fichier typeScript, tout les modif seront envoyées au server.js automatiquement.
    
    - `server.js` qui sera un fichier javaScript, qui prendra les modif ajoutées dans server.ts.

## Code de base

- `[voici un code de base pour faire fonctioner un server]` 

```javascript
//1
import express from 'express';

//2
const app = express();

//3
app.get('/api', (req, res) => {
    return res.send('hello world');
});

//4
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
```

1. En premier lieu nous allons importer `express` qui est un outil de node pour créer un serveur.

2. Puis créer une variable `app` égale à `express`.

3. Ici, nous allons découvrir le premier verbe `get` qui permet d'obtenir une data.

    Donc lorsque l'on rentre l'url `/api` nous allons obtenir la requête avec `req` et envoyer une respond avec `res`.
    
    Ainsi, nous allons return `res.send` pour renvoyer la réponse ('hello world').
    
4. Et en dernière partie, `app.listen` qui définira le port et `console.log` le lien pour accéder au serveurs.

