###  GET 

La syntaxe `GET`  permet de lier une propriété d'un objet à une fonction qui sera appelée lorsqu'on accédera à la propriété.

Les requêtes `GET` doivent uniquement être utilisées afin de récupérer des données.

```javascript
const obj = {
  log: ['a', 'b', 'c'],
  get latest() {
    if (this.log.length == 0) {
      return undefined;
    }
    return this.log[this.log.length - 1];
  }
}
console.log(obj.latest);
```
// expected output: "c"

### DELETE

L'opérateur `DELETE`  permet de retirer une propriété d'un objet.

```javascript

const Employee = {
  firstname: 'John',
  lastname: 'Doe'
}

console.log(Employee.firstname);
// expected output: "John"

delete Employee.firstname;

console.log(Employee.firstname);
// expected output: undefined
```

###  PATCH

La méthode `PATCH` est utilisée pour appliquer des modifications partielles à une ressource.

### POST

La méthode `POST` est utilisée pour envoyer une entité vers la ressource indiquée. Cela entraîne généralement un changement d'état ou des effets de bord sur le serveur.

### PUT

La méthode `PUT` remplace toutes les représentations actuelles de la ressource visée par le contenu de la requête.
