# ROW & COL

>Il est formelement interdit d'avoir un row san col 

>interdit de faire un row seul une col toute seule un row sans col 

**exemple**
```html
<div class="container">    
    <div class="row">
        <div class="col"></div>
        <div class="col"></div>
    </div>
</div>
```
>il y a 12 col possibles 

**exemple**
```html
<div class="container">    
    <div class="row">
        <div class="col-12"></div>
        <div class="col-12"></div>
    </div>
</div>
```

>pour decaler un col on utilise (offset)

**exemple**

```html
<div class="container">    
    <div class="row">
        <div class="col-3 offset-1"></div>
        <div class="col-7 offset-1"></div>
    </div>
</div>
```
>pour les col apparaissent il faut un contenu  