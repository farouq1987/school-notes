# Bootstrap

>bootstrap est une boîte à outils open source pour le développement avec HTML, CSS et JS
>le plus utilisé au monde

 [bootstrap](https://getbootstrap.com)

 ###installation
 
**Classement d'un site internet avec bootstrap**
  
1. création d'un projet (exemple:le projet My-cv)

2. les bonnes pratiques : on doit couper le projet a plusieurs directory (dossier) ex : directory (main) on trouve index.html la page principale et assets on trouve la partie css ainsi de suite

**Installation**

1. on ouvre la partie terminal (c'est un outil qui permt de passer des commandes à l'ordinateur avec du texte uniquement)

2. la commande npm init (npm=c'est un logiciel ou un store que l'on doit installer "node"  )

3. on fait entrer =suivant (on va remarquer que y'a des fichiers qui sont creés dans mon projet package json)

4.  npm install bootstrap ( installation )

on va remarquer que on a directory (node modules) qui contient  le directory bootstrap

**Activation de bootstrap**

>ex: dans directory main on ouvre la page index.html
> activation bootstrap avec la balise link exemple:

```<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css>```
 
**Utilisation**

>dans le directory css on va créer une page styles.css qui va être liée à la page index.html

```<link rel="stylesheet" href="../css/styles.css>```

**Modifier bootstrap**


important : ``Ne jamais modifier des fichiers vendors``

* Modifier le bootstrap en ajoutant une classe au container

1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple

2. Faire un import de ce fichier dans le fichier styles.css

3. Importer aussi le styles.css dans le fichier html

4. Rajouter une classe my-container au container

5. Apporter les modifications voulu sur le fichier mod.bootstrap.css a l'aide de la classe créé


* Modifier le bootstrap directement dans le container

1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple
2. Faire un import de ce fichier dans le fichier styles.css
3. Importer aussi le styles.css dans le fichier html
4. Modifier .container sans créer la classe my-container
5. Apporter les modifications voulu sur le fichier mod.bootstrap.css a l'aide de .container directement

Pour modifier le width d'une col qui est déja fixé par le fichier bootstrap, il faut mettre les liens css après bootstrap et mettre son propre fichier css avant.
