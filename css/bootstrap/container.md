# Container

il y a deux types de container 

1. **container** :(ou le container classique permet d'afficher le contenu du site au centre de la page et les cotés sont des parties vides) 

2. **container_fluid** :(le container_fluid prend l'integralité de la page sur la largeur)

### Exemple


**container**

```html
<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
```
**container_fluid**


```html
<div class="container-fluid">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
```